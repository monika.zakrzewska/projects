﻿namespace sprawdzian_2
{
    partial class Sprawdzian
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButton_zad1_odp1 = new System.Windows.Forms.RadioButton();
            this.label_zad1 = new System.Windows.Forms.Label();
            this.radioButton_zad1_odp2 = new System.Windows.Forms.RadioButton();
            this.radioButton_zad1_odp3 = new System.Windows.Forms.RadioButton();
            this.radioButton_zad1_odp4 = new System.Windows.Forms.RadioButton();
            this.groupBox_zad1 = new System.Windows.Forms.GroupBox();
            this.label_zad1_odp = new System.Windows.Forms.Label();
            this.button_zad1_odp = new System.Windows.Forms.Button();
            this.groupBox_zad2 = new System.Windows.Forms.GroupBox();
            this.label_zad2_odp = new System.Windows.Forms.Label();
            this.button_zad2_odp = new System.Windows.Forms.Button();
            this.comboBox_zad2_odp = new System.Windows.Forms.ComboBox();
            this.label_zad2 = new System.Windows.Forms.Label();
            this.groupBox_zad3 = new System.Windows.Forms.GroupBox();
            this.label_zad3_odp = new System.Windows.Forms.Label();
            this.button_zad3_odp = new System.Windows.Forms.Button();
            this.textBox_zad3_odp = new System.Windows.Forms.TextBox();
            this.label_zad31 = new System.Windows.Forms.Label();
            this.label_zad3 = new System.Windows.Forms.Label();
            this.button_zamknij = new System.Windows.Forms.Button();
            this.groupBox_zad4 = new System.Windows.Forms.GroupBox();
            this.button_zad4 = new System.Windows.Forms.Button();
            this.label_zad4 = new System.Windows.Forms.Label();
            this.label_zad41 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label_zad4_odp = new System.Windows.Forms.Label();
            this.groupBox_zad5 = new System.Windows.Forms.GroupBox();
            this.numeric_zad5 = new System.Windows.Forms.NumericUpDown();
            this.label_zad5 = new System.Windows.Forms.Label();
            this.button_zad5 = new System.Windows.Forms.Button();
            this.label_zad5_odp = new System.Windows.Forms.Label();
            this.button_spr = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.informacjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informacjeOQuizieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informacjeOAutorzeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox_zad1.SuspendLayout();
            this.groupBox_zad2.SuspendLayout();
            this.groupBox_zad3.SuspendLayout();
            this.groupBox_zad4.SuspendLayout();
            this.groupBox_zad5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_zad5)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButton_zad1_odp1
            // 
            this.radioButton_zad1_odp1.AutoSize = true;
            this.radioButton_zad1_odp1.Location = new System.Drawing.Point(41, 43);
            this.radioButton_zad1_odp1.Name = "radioButton_zad1_odp1";
            this.radioButton_zad1_odp1.Size = new System.Drawing.Size(68, 17);
            this.radioButton_zad1_odp1.TabIndex = 0;
            this.radioButton_zad1_odp1.TabStop = true;
            this.radioButton_zad1_odp1.Text = "około 60";
            this.radioButton_zad1_odp1.UseVisualStyleBackColor = true;
            // 
            // label_zad1
            // 
            this.label_zad1.AutoSize = true;
            this.label_zad1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_zad1.Location = new System.Drawing.Point(15, 16);
            this.label_zad1.Name = "label_zad1";
            this.label_zad1.Size = new System.Drawing.Size(164, 15);
            this.label_zad1.TabIndex = 1;
            this.label_zad1.Text = "W ilu krajach działa IAESTE?";
            // 
            // radioButton_zad1_odp2
            // 
            this.radioButton_zad1_odp2.AutoSize = true;
            this.radioButton_zad1_odp2.Location = new System.Drawing.Point(41, 66);
            this.radioButton_zad1_odp2.Name = "radioButton_zad1_odp2";
            this.radioButton_zad1_odp2.Size = new System.Drawing.Size(68, 17);
            this.radioButton_zad1_odp2.TabIndex = 2;
            this.radioButton_zad1_odp2.TabStop = true;
            this.radioButton_zad1_odp2.Text = "około 70";
            this.radioButton_zad1_odp2.UseVisualStyleBackColor = true;
            // 
            // radioButton_zad1_odp3
            // 
            this.radioButton_zad1_odp3.AutoSize = true;
            this.radioButton_zad1_odp3.BackColor = System.Drawing.Color.Transparent;
            this.radioButton_zad1_odp3.Location = new System.Drawing.Point(41, 89);
            this.radioButton_zad1_odp3.Name = "radioButton_zad1_odp3";
            this.radioButton_zad1_odp3.Size = new System.Drawing.Size(68, 17);
            this.radioButton_zad1_odp3.TabIndex = 3;
            this.radioButton_zad1_odp3.TabStop = true;
            this.radioButton_zad1_odp3.Text = "około 75";
            this.radioButton_zad1_odp3.UseVisualStyleBackColor = false;
            // 
            // radioButton_zad1_odp4
            // 
            this.radioButton_zad1_odp4.AutoSize = true;
            this.radioButton_zad1_odp4.Location = new System.Drawing.Point(41, 112);
            this.radioButton_zad1_odp4.Name = "radioButton_zad1_odp4";
            this.radioButton_zad1_odp4.Size = new System.Drawing.Size(68, 17);
            this.radioButton_zad1_odp4.TabIndex = 4;
            this.radioButton_zad1_odp4.TabStop = true;
            this.radioButton_zad1_odp4.Text = "około 85";
            this.radioButton_zad1_odp4.UseVisualStyleBackColor = true;
            // 
            // groupBox_zad1
            // 
            this.groupBox_zad1.Controls.Add(this.label_zad1_odp);
            this.groupBox_zad1.Controls.Add(this.button_zad1_odp);
            this.groupBox_zad1.Controls.Add(this.radioButton_zad1_odp4);
            this.groupBox_zad1.Controls.Add(this.radioButton_zad1_odp3);
            this.groupBox_zad1.Controls.Add(this.radioButton_zad1_odp2);
            this.groupBox_zad1.Controls.Add(this.label_zad1);
            this.groupBox_zad1.Controls.Add(this.radioButton_zad1_odp1);
            this.groupBox_zad1.Location = new System.Drawing.Point(12, 27);
            this.groupBox_zad1.Name = "groupBox_zad1";
            this.groupBox_zad1.Size = new System.Drawing.Size(191, 189);
            this.groupBox_zad1.TabIndex = 5;
            this.groupBox_zad1.TabStop = false;
            this.groupBox_zad1.Text = "Zadanie 1";
            // 
            // label_zad1_odp
            // 
            this.label_zad1_odp.AutoSize = true;
            this.label_zad1_odp.Location = new System.Drawing.Point(22, 164);
            this.label_zad1_odp.Name = "label_zad1_odp";
            this.label_zad1_odp.Size = new System.Drawing.Size(0, 13);
            this.label_zad1_odp.TabIndex = 6;
            // 
            // button_zad1_odp
            // 
            this.button_zad1_odp.Location = new System.Drawing.Point(18, 135);
            this.button_zad1_odp.Name = "button_zad1_odp";
            this.button_zad1_odp.Size = new System.Drawing.Size(145, 22);
            this.button_zad1_odp.TabIndex = 5;
            this.button_zad1_odp.Text = "Zatwierdź";
            this.button_zad1_odp.UseVisualStyleBackColor = true;
            this.button_zad1_odp.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox_zad2
            // 
            this.groupBox_zad2.Controls.Add(this.label_zad2_odp);
            this.groupBox_zad2.Controls.Add(this.button_zad2_odp);
            this.groupBox_zad2.Controls.Add(this.comboBox_zad2_odp);
            this.groupBox_zad2.Controls.Add(this.label_zad2);
            this.groupBox_zad2.Location = new System.Drawing.Point(209, 27);
            this.groupBox_zad2.Name = "groupBox_zad2";
            this.groupBox_zad2.Size = new System.Drawing.Size(208, 189);
            this.groupBox_zad2.TabIndex = 6;
            this.groupBox_zad2.TabStop = false;
            this.groupBox_zad2.Text = "Zadanie 2";
            // 
            // label_zad2_odp
            // 
            this.label_zad2_odp.AutoSize = true;
            this.label_zad2_odp.Location = new System.Drawing.Point(16, 123);
            this.label_zad2_odp.Name = "label_zad2_odp";
            this.label_zad2_odp.Size = new System.Drawing.Size(0, 13);
            this.label_zad2_odp.TabIndex = 3;
            // 
            // button_zad2_odp
            // 
            this.button_zad2_odp.Location = new System.Drawing.Point(14, 89);
            this.button_zad2_odp.Name = "button_zad2_odp";
            this.button_zad2_odp.Size = new System.Drawing.Size(166, 19);
            this.button_zad2_odp.TabIndex = 2;
            this.button_zad2_odp.Text = "Zatwierdź";
            this.button_zad2_odp.UseVisualStyleBackColor = true;
            this.button_zad2_odp.Click += new System.EventHandler(this.button_zad2_odp_Click);
            // 
            // comboBox_zad2_odp
            // 
            this.comboBox_zad2_odp.FormattingEnabled = true;
            this.comboBox_zad2_odp.Items.AddRange(new object[] {
            "1946",
            "1948",
            "1952",
            "1959"});
            this.comboBox_zad2_odp.Location = new System.Drawing.Point(14, 53);
            this.comboBox_zad2_odp.Name = "comboBox_zad2_odp";
            this.comboBox_zad2_odp.Size = new System.Drawing.Size(167, 21);
            this.comboBox_zad2_odp.TabIndex = 1;
            this.comboBox_zad2_odp.Text = "Wybierz rok";
            // 
            // label_zad2
            // 
            this.label_zad2.AutoSize = true;
            this.label_zad2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_zad2.Location = new System.Drawing.Point(11, 26);
            this.label_zad2.Name = "label_zad2";
            this.label_zad2.Size = new System.Drawing.Size(187, 15);
            this.label_zad2.TabIndex = 0;
            this.label_zad2.Text = "W którym roku powstało IAESTE?";
            // 
            // groupBox_zad3
            // 
            this.groupBox_zad3.Controls.Add(this.label_zad3_odp);
            this.groupBox_zad3.Controls.Add(this.button_zad3_odp);
            this.groupBox_zad3.Controls.Add(this.textBox_zad3_odp);
            this.groupBox_zad3.Controls.Add(this.label_zad31);
            this.groupBox_zad3.Controls.Add(this.label_zad3);
            this.groupBox_zad3.Location = new System.Drawing.Point(423, 27);
            this.groupBox_zad3.Name = "groupBox_zad3";
            this.groupBox_zad3.Size = new System.Drawing.Size(312, 189);
            this.groupBox_zad3.TabIndex = 7;
            this.groupBox_zad3.TabStop = false;
            this.groupBox_zad3.Text = "Zadanie 3";
            // 
            // label_zad3_odp
            // 
            this.label_zad3_odp.AutoSize = true;
            this.label_zad3_odp.Location = new System.Drawing.Point(20, 133);
            this.label_zad3_odp.Name = "label_zad3_odp";
            this.label_zad3_odp.Size = new System.Drawing.Size(0, 13);
            this.label_zad3_odp.TabIndex = 4;
            // 
            // button_zad3_odp
            // 
            this.button_zad3_odp.Location = new System.Drawing.Point(23, 102);
            this.button_zad3_odp.Name = "button_zad3_odp";
            this.button_zad3_odp.Size = new System.Drawing.Size(246, 19);
            this.button_zad3_odp.TabIndex = 3;
            this.button_zad3_odp.Text = "Zatwierdź";
            this.button_zad3_odp.UseVisualStyleBackColor = true;
            this.button_zad3_odp.Click += new System.EventHandler(this.button_zad3_odp_Click);
            // 
            // textBox_zad3_odp
            // 
            this.textBox_zad3_odp.Location = new System.Drawing.Point(23, 68);
            this.textBox_zad3_odp.Name = "textBox_zad3_odp";
            this.textBox_zad3_odp.Size = new System.Drawing.Size(246, 20);
            this.textBox_zad3_odp.TabIndex = 2;
            this.textBox_zad3_odp.Text = "Wprowdź imię i nazwisko";
            this.textBox_zad3_odp.Click += new System.EventHandler(this.textBox_zad3_odp_Click);
            // 
            // label_zad31
            // 
            this.label_zad31.AutoSize = true;
            this.label_zad31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_zad31.Location = new System.Drawing.Point(22, 47);
            this.label_zad31.Name = "label_zad31";
            this.label_zad31.Size = new System.Drawing.Size(107, 13);
            this.label_zad31.TabIndex = 1;
            this.label_zad31.Text = "Podaj imię i nazwisko";
            // 
            // label_zad3
            // 
            this.label_zad3.AutoSize = true;
            this.label_zad3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_zad3.Location = new System.Drawing.Point(22, 26);
            this.label_zad3.Name = "label_zad3";
            this.label_zad3.Size = new System.Drawing.Size(278, 15);
            this.label_zad3.TabIndex = 0;
            this.label_zad3.Text = "Jak się nazywa obecny prezydent IAESTE Polska?";
            // 
            // button_zamknij
            // 
            this.button_zamknij.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_zamknij.Location = new System.Drawing.Point(610, 355);
            this.button_zamknij.Name = "button_zamknij";
            this.button_zamknij.Size = new System.Drawing.Size(125, 65);
            this.button_zamknij.TabIndex = 8;
            this.button_zamknij.Text = "Zamknij";
            this.button_zamknij.UseVisualStyleBackColor = true;
            this.button_zamknij.Click += new System.EventHandler(this.button_zamknij_Click);
            // 
            // groupBox_zad4
            // 
            this.groupBox_zad4.Controls.Add(this.label_zad4_odp);
            this.groupBox_zad4.Controls.Add(this.dateTimePicker1);
            this.groupBox_zad4.Controls.Add(this.label_zad41);
            this.groupBox_zad4.Controls.Add(this.button_zad4);
            this.groupBox_zad4.Controls.Add(this.label_zad4);
            this.groupBox_zad4.Location = new System.Drawing.Point(12, 226);
            this.groupBox_zad4.Name = "groupBox_zad4";
            this.groupBox_zad4.Size = new System.Drawing.Size(191, 213);
            this.groupBox_zad4.TabIndex = 9;
            this.groupBox_zad4.TabStop = false;
            this.groupBox_zad4.Text = "Zadanie4";
            // 
            // button_zad4
            // 
            this.button_zad4.Location = new System.Drawing.Point(18, 133);
            this.button_zad4.Name = "button_zad4";
            this.button_zad4.Size = new System.Drawing.Size(153, 25);
            this.button_zad4.TabIndex = 3;
            this.button_zad4.Text = "Zatwierdź";
            this.button_zad4.UseVisualStyleBackColor = true;
            this.button_zad4.Click += new System.EventHandler(this.button_zad4_Click);
            // 
            // label_zad4
            // 
            this.label_zad4.AutoSize = true;
            this.label_zad4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_zad4.Location = new System.Drawing.Point(15, 26);
            this.label_zad4.Name = "label_zad4";
            this.label_zad4.Size = new System.Drawing.Size(159, 30);
            this.label_zad4.TabIndex = 0;
            this.label_zad4.Text = "W jakim dniu odbyła się \r\nKonferencja Krajowa 2020?\r\n";
            // 
            // label_zad41
            // 
            this.label_zad41.AutoSize = true;
            this.label_zad41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_zad41.Location = new System.Drawing.Point(15, 65);
            this.label_zad41.Name = "label_zad41";
            this.label_zad41.Size = new System.Drawing.Size(69, 13);
            this.label_zad41.TabIndex = 5;
            this.label_zad41.Text = "Wybierz date";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(18, 94);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(153, 20);
            this.dateTimePicker1.TabIndex = 6;
            // 
            // label_zad4_odp
            // 
            this.label_zad4_odp.AutoSize = true;
            this.label_zad4_odp.Location = new System.Drawing.Point(16, 173);
            this.label_zad4_odp.Name = "label_zad4_odp";
            this.label_zad4_odp.Size = new System.Drawing.Size(0, 13);
            this.label_zad4_odp.TabIndex = 7;
            // 
            // groupBox_zad5
            // 
            this.groupBox_zad5.Controls.Add(this.label_zad5_odp);
            this.groupBox_zad5.Controls.Add(this.button_zad5);
            this.groupBox_zad5.Controls.Add(this.label_zad5);
            this.groupBox_zad5.Controls.Add(this.numeric_zad5);
            this.groupBox_zad5.Location = new System.Drawing.Point(209, 226);
            this.groupBox_zad5.Name = "groupBox_zad5";
            this.groupBox_zad5.Size = new System.Drawing.Size(208, 213);
            this.groupBox_zad5.TabIndex = 10;
            this.groupBox_zad5.TabStop = false;
            this.groupBox_zad5.Text = "Zadanie 5";
            // 
            // numeric_zad5
            // 
            this.numeric_zad5.Location = new System.Drawing.Point(42, 79);
            this.numeric_zad5.Name = "numeric_zad5";
            this.numeric_zad5.Size = new System.Drawing.Size(120, 20);
            this.numeric_zad5.TabIndex = 0;
            this.numeric_zad5.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label_zad5
            // 
            this.label_zad5.AutoSize = true;
            this.label_zad5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_zad5.Location = new System.Drawing.Point(25, 29);
            this.label_zad5.Name = "label_zad5";
            this.label_zad5.Size = new System.Drawing.Size(155, 30);
            this.label_zad5.TabIndex = 1;
            this.label_zad5.Text = "Ile członków zawiera Biuro \r\nNarodowe IAESTE Polska?";
            // 
            // button_zad5
            // 
            this.button_zad5.Location = new System.Drawing.Point(26, 127);
            this.button_zad5.Name = "button_zad5";
            this.button_zad5.Size = new System.Drawing.Size(153, 23);
            this.button_zad5.TabIndex = 2;
            this.button_zad5.Text = "Zatwierdź";
            this.button_zad5.UseVisualStyleBackColor = true;
            this.button_zad5.Click += new System.EventHandler(this.button_zad5_Click);
            // 
            // label_zad5_odp
            // 
            this.label_zad5_odp.AutoSize = true;
            this.label_zad5_odp.Location = new System.Drawing.Point(24, 172);
            this.label_zad5_odp.Name = "label_zad5_odp";
            this.label_zad5_odp.Size = new System.Drawing.Size(0, 13);
            this.label_zad5_odp.TabIndex = 3;
            // 
            // button_spr
            // 
            this.button_spr.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_spr.Location = new System.Drawing.Point(455, 250);
            this.button_spr.Name = "button_spr";
            this.button_spr.Size = new System.Drawing.Size(279, 70);
            this.button_spr.TabIndex = 11;
            this.button_spr.Text = "Sprawdź swój wynik!";
            this.button_spr.UseVisualStyleBackColor = true;
            this.button_spr.Click += new System.EventHandler(this.button_spr_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informacjeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(775, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // informacjeToolStripMenuItem
            // 
            this.informacjeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informacjeOQuizieToolStripMenuItem,
            this.informacjeOAutorzeToolStripMenuItem});
            this.informacjeToolStripMenuItem.Name = "informacjeToolStripMenuItem";
            this.informacjeToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.informacjeToolStripMenuItem.Text = "Informacje";
            // 
            // informacjeOQuizieToolStripMenuItem
            // 
            this.informacjeOQuizieToolStripMenuItem.Name = "informacjeOQuizieToolStripMenuItem";
            this.informacjeOQuizieToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.informacjeOQuizieToolStripMenuItem.Text = "Informacje o quizie";
            this.informacjeOQuizieToolStripMenuItem.Click += new System.EventHandler(this.informacjeOQuizieToolStripMenuItem_Click);
            // 
            // informacjeOAutorzeToolStripMenuItem
            // 
            this.informacjeOAutorzeToolStripMenuItem.Name = "informacjeOAutorzeToolStripMenuItem";
            this.informacjeOAutorzeToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.informacjeOAutorzeToolStripMenuItem.Text = "Informacje o autorze";
            this.informacjeOAutorzeToolStripMenuItem.Click += new System.EventHandler(this.informacjeOAutorzeToolStripMenuItem_Click);
            // 
            // Sprawdzian
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(775, 451);
            this.Controls.Add(this.button_spr);
            this.Controls.Add(this.groupBox_zad5);
            this.Controls.Add(this.groupBox_zad4);
            this.Controls.Add(this.button_zamknij);
            this.Controls.Add(this.groupBox_zad3);
            this.Controls.Add(this.groupBox_zad2);
            this.Controls.Add(this.groupBox_zad1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Sprawdzian";
            this.Text = "Quiz o IAESTE";
            this.groupBox_zad1.ResumeLayout(false);
            this.groupBox_zad1.PerformLayout();
            this.groupBox_zad2.ResumeLayout(false);
            this.groupBox_zad2.PerformLayout();
            this.groupBox_zad3.ResumeLayout(false);
            this.groupBox_zad3.PerformLayout();
            this.groupBox_zad4.ResumeLayout(false);
            this.groupBox_zad4.PerformLayout();
            this.groupBox_zad5.ResumeLayout(false);
            this.groupBox_zad5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_zad5)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButton_zad1_odp1;
        private System.Windows.Forms.Label label_zad1;
        private System.Windows.Forms.RadioButton radioButton_zad1_odp2;
        private System.Windows.Forms.RadioButton radioButton_zad1_odp3;
        private System.Windows.Forms.RadioButton radioButton_zad1_odp4;
        private System.Windows.Forms.GroupBox groupBox_zad1;
        private System.Windows.Forms.GroupBox groupBox_zad2;
        private System.Windows.Forms.GroupBox groupBox_zad3;
        private System.Windows.Forms.TextBox textBox_zad3_odp;
        private System.Windows.Forms.Label label_zad31;
        private System.Windows.Forms.Label label_zad3;
        private System.Windows.Forms.Button button_zad1_odp;
        private System.Windows.Forms.Label label_zad1_odp;
        private System.Windows.Forms.Label label_zad3_odp;
        private System.Windows.Forms.Button button_zad3_odp;
        private System.Windows.Forms.Button button_zamknij;
        private System.Windows.Forms.Label label_zad2_odp;
        private System.Windows.Forms.Button button_zad2_odp;
        private System.Windows.Forms.ComboBox comboBox_zad2_odp;
        private System.Windows.Forms.Label label_zad2;
        private System.Windows.Forms.GroupBox groupBox_zad4;
        private System.Windows.Forms.Label label_zad4;
        private System.Windows.Forms.Button button_zad4;
        private System.Windows.Forms.Label label_zad4_odp;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label_zad41;
        private System.Windows.Forms.GroupBox groupBox_zad5;
        private System.Windows.Forms.NumericUpDown numeric_zad5;
        private System.Windows.Forms.Label label_zad5_odp;
        private System.Windows.Forms.Button button_zad5;
        private System.Windows.Forms.Label label_zad5;
        private System.Windows.Forms.Button button_spr;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem informacjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informacjeOQuizieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informacjeOAutorzeToolStripMenuItem;
    }
}

