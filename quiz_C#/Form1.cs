﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sprawdzian_2
{
    public partial class Sprawdzian : Form
    {
        public int tmp { get; private set; }

        public Sprawdzian()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton_zad1_odp4.Checked == true)
            {
                //label_zad1_odp.Text = "Brawo! Poprawna odpowiedź!";
                tmp += 1;
            }
            /*else
            {
                label_zad1_odp.Text = "Niestety zła odpowiedź :(";
            }*/
        }

        private void button_zad3_odp_Click(object sender, EventArgs e)
        {
            if (textBox_zad3_odp.Text == "Przemysław Bieniasz")
            {
                //label_zad3_odp.Text = "Brawo! Poprawna odpowiedź!";
                tmp += 1;
            }
            /*else
            {
                label_zad3_odp.Text = "Niestety zła odpowiedź :(";
            }*/
        }

        private void textBox_zad3_odp_Click(object sender, EventArgs e)
        {
            textBox_zad3_odp.Text = "";
        }

        private void button_zamknij_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_zad2_odp_Click(object sender, EventArgs e)
        {
            if (comboBox_zad2_odp.Text == "1948")
            {
                //label_zad2_odp.Text = "Brawo! Poprawna odpowiedź!";
                tmp += 1;
            }
            /*else
            {
                label_zad2_odp.Text = "Niestey zła odpowiedź :(";
            }*/
        }

        private void button_zad4_Click(object sender, EventArgs e)
        {
            if (dateTimePicker1.Value == Convert.ToDateTime("08-02-2020"))
            {
                //label_zad4_odp.Text = "Brawo! Poprawna odpowiedź!";
                tmp += 1;
            }
            /*else
            {
                label_zad4_odp.Text = "Niestey zła odpowiedź :(";
            }*/
        }

        private void button_zad5_Click(object sender, EventArgs e)
        {
            if (numeric_zad5.Value == 10)
            {
                //label_zad5_odp.Text = "Brawo! Poprawna odpowiedź!";
                tmp += 1;
            }
            /*else
            {
                label_zad5_odp.Text = "Niestey zła odpowiedź :(";
            }*/
        }

        private void button_spr_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Brawo! \n" + tmp + "/5", "Wynik", MessageBoxButtons.OK);
        }

        private void informacjeOAutorzeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Monika Zakrzewska \n" + "172605", "Informacje o autorze", MessageBoxButtons.OK);
        }

        private void informacjeOQuizieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Kilka krótkich pytań o IAESTE :) \n" + "Klikając odpowiedzi w quizie \n" + "nazleży je zatwierdzić, aby \n"
                + "znać swój wynik :) \n" + "Każde pytanie jest za 1 punkt.",
                "Informacje o quizie", MessageBoxButtons.OK);
        }
    }
}
