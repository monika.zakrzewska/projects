# Politechnika Gdańska
# Wydział Fizyki Technicznej i Matematyki Stosowanej
# Matematyka stosowana, 2 rok
# Laboratorium 2: Algorytmy sortujące
author = "Monika Zakrzewska"


# 13.03.2019

# lista = list(range(0, 100001)) lista wszystkich liczb od 0 do 100000
# print(lista)

shellcount = 0


def shellsort(lista):  # definiujemy funkcje ShellSort
    global shellcount

    podlista = len(lista) // 2  # generujemy podlistę, len() zwraca ilość elementów,
    # a //2 robi podłogę z podzielenia danej listy elementów przez 2
    while podlista > 0:  # wstawiamy pętlę while - dopóki wartość podlisty będzie większa od 0,
        # jeśli okazałaby się mniejsza, bądź równa 0, to pętla się zatrzymuje
        for start in range(podlista):  # umieszczamy pętlę for dla iterowania po elementach podlisty,
                                        # funkcja wbudowana range służy do wypisania listy ciągu arytmetycznego
            # funkcje robimy dla zmiennej start
            # inaczej: dla każdego start w ciągu arytmetycznym wygenerowanym z podlisty

            #shellcount +=
            przerwa(lista, start, podlista)  # przerwa zdefiniowana poniżej

        print("po przyroście ", podlista, "lista wygląda tak: ", lista)
        podlista = podlista // 2  # ponownie robimy podłogę z podlisty, którą właśnie posortowaliśmy i algorytm sie powtarza
    print("liczba operacji: ", shellcount)


def przerwa(lista, start, przyrost):  # definujemy przerwe
    global shellcount
    for i in range(start + przyrost, len(lista), przyrost):  # dla każdej liczbry i (tak jakby liczby porządkowej)
        # w ciągu arytmetycznym od start+przyrost do długośći listy
        # i kroku o wielkości przyrost
        wartosc = lista[i]  # oddaje wartość z listy dla danego i
        pozycja = i  # pozycja jest liczbą porządkową
        while pozycja >= przyrost and lista[pozycja - przyrost] > wartosc:  # pętla while działa dopóki pozycja (,czyli i) jest więsza bądź równa przyrostowi
            # i równocześnie lista, która ma indeks o wartości pozycja - przyrost
            # jest większa od wartości,
            # aby pętla działała obydwa warunki muszą być spełnione
            lista[pozycja] = lista[pozycja - przyrost]  # zmieniamy indeks listy na mniejszy
            pozycja = pozycja - przyrost  # pokazujemy jak "liczymy" pozycje
            shellcount += 1
        lista[pozycja] = wartosc  # pokazujemy czym jest teraz wartosć
    return shellcount

mergecount = 0


def mergesort(lista):  # definiujemy MergeSort
    global mergecount
    print("Rozdzielamy", lista)
    if len(lista) > 1:
        srodek = len(lista) // 2  # określamy, gdzie jest środek (z podłogą)
        lewo = lista[:srodek]  # przypisujemy lewej stronie liczby od pozycji 0 do srodka
        prawo = lista[srodek:]  # przypisujemy prawej stronie liczby od pozycji srodkowej do końca
        mergesort(lewo)  # powtarzamy dla lewej strony, jeśli >1
        mergesort(prawo)  # powtarzamy dla prawej strony, jeśli >1
        k = 0  # zaczynamy od indeksu 0
        p = 0
        m = 0
        while k < len(lewo) and p < len(prawo):  # pęla while działa do czasu, aż jeden z dwóch warunków nie będzie spełniony
            if lewo[k] < prawo[p]:  # jeśli wartość po lewej stronie jest mniejsza niż po prawej, to
                lista[m] = lewo[k]  # odpowiednio zaindeksowana część listy całej, to część lewo(w zależności od indeksu k)
                k += 1  # ineks powiksza się o 1 z każdym razem, gdy "przejdzie" pętla
                #mergecount += 1
            else:
                lista[m] = prawo[p]  # w innym wypadku odpowiednio zaindeksowana część listy całej, to część prawo(w zależności od indeksu p)
                p += 1
                #mergecount += 1
            m += 1  # powiększamy listę o 1 za każdym razem

        while k < len(lewo):  # dopóki indeks k jest mniejszy od długości części lewej, to
            lista[m] = lewo[k]  # lista odpowiednio zaindeksowana, to część lewo(w zależności od indeksu k)
            k += 1  # k zwiększa się o 1 za każdym razem
            m += 1  # m zwiększa się o 1 za każdym razem
            mergecount += 1
        while p < len(prawo):  # dopoki indeks p jest mniejszy od dlugosci czesci prawej, to
            lista[m] = prawo[p]  # lista odpowiednio zaindeksowana, to część prawo(w zależności od indeksu p)
            p += 1  # n zwiększa się o 1 za każdym razem
            m += 1  # m zwiększa się o 1 za każdym razem
            mergecount += 1
        #mergecount += 1
    else:  # w innym przypadku
        return lista

    # mergecount += 1 czy to powinno być?
    print("posortowano: ", lista)
    print("ile?", mergecount)

quickcount = 0


def quicksort(lista):  # definiujemy Quicksort
    global quickcount
    # quickcount = 0
    pomoc(lista, 0, len(lista) - 1)  # dajemy sobie funkcje pomocniczą, która początek ma w liczbie 0, a koniec w długości listy-1
    poczatek = 0  # oznaczamy ile ma poczatek
    koniec = len(lista) - 1  # oznaczamy ile ma koniec
    #quickcount += pomoc(lista, poczatek, koniec)
    print("ile?:", quickcount)


#qcount1 = 0


def pomoc(lista, poczatek, koniec):  # definiujemy pomocniczą
    #global qcount1
    # qcount1 = 0
    if poczatek < koniec:  # jeśli początek listy jest mniejszy od końca listy, to
        punkt = podzial(lista, poczatek, koniec)  # robimy sobie pewnien punkt, gdzie robimy podział(zdefiniowany poniżej)
        pomoc(lista, poczatek, punkt - 1)  # najpierw dajemy z listy od począteku do danego punktu -1
        pomoc(lista, punkt + 1, koniec)  # później dajemy  listy od danego punktu +1 do końca

        # qcount1 #+= #podzial(lista, poczatek, koniec)#podzial(lista, poczatek, koniec)
        #qcount1 += 1
    #return qcount1


#qcount = 0


def podzial(lista, poczatek, koniec):  # definiujemy podział
    global quickcount
    # qcount = 0
    pivot = lista[poczatek]  # "obrót" listy z indeksami
    # dzielimy sobie listę na lewą i prawą
    lewa = poczatek + 1  # do lewej dodajemy 1, bo początek(na początku) = 0
    prawa = koniec  # prawej dajemy koniec, żeby wszystko było ciągłe
    zrobione = False  # dajemy, że jest to fałsz, ponieważ
    while not zrobione:  # dopóki nie jest to zrobione(czyli dopóki jest prawdziwe), to
        while lewa <= prawa and lista[lewa] <= pivot:  # dopóki oba warunki są spełnione, to
            lewa += 1  # lewwą stronę powiększamy ciągle o 1
            #quickcount += 1
        while lewa <= prawa and lista[prawa] >= pivot:  # dopóki oba warunki są spełnione, to
            prawa -= 1  # od prawej strony ciągle odejmujemy 1
            #quickcount += 1
        if prawa < lewa:  # jeśli lewa jest więsza od prawej, to
            zrobione = True  # zrobione staje się prawdą, więc całość staje się fałszem i jest prawdziwe
            quickcount += 1
        else:  # w innym przypadku
            x = lista[lewa]  # jakaś zmienna jest lewą częścią listy
            lista[lewa] = lista[prawa]  # lewa część listy ma tyle samo co prawa
            lista[prawa] = x  # prawa część listy jest również jakąś zmienną x
            quickcount += 1
    x = lista[poczatek]  # x jest równa elementowi listy o danym indeksie początkowym
    lista[poczatek] = lista[prawa]  # element listy o danym indeksie początkowym jest równy elementowi z listy prawej
    lista[prawa] = x  # element z listy prawej równa się x

    return prawa  # zwracamy prawą wartość


from random import randint  # z modułu random importujemy funkcję randint(a, b), która służy do generowania liczb z przedziału [a, b]

print("Podaj ilość liczb do wylosowania z przedziału [0,100000]")
n = int(input("n =  "))  # wpisujemy ile liczb chcemy wywołać
lista = []  # pusta lista, gdzie można wpisać wylosowane liczby
for j in range(0, n):
    lista.append(randint(0, 100001))  # losowanie danych n liczb z przedziału od 0 do 100000 i tworzymy liste n-elementową
                                      # funkcja .append dodaje elementy do listy
print("lista wygląda tak: ", lista)

lista1 = []
for j in range(0, 100001):
    lista1.append(j)
logfile = open('lista.txt', 'w')  # zapisywanie do pliku lista.txt
print(lista1, file=logfile)
logfile.close()

print("1. ShellSort")
print("2. MergeSort")
print("3. QuickSort")

a = int(input("Którą opcje sortowania wybierasz? \n"))

import time



while a >= 1 and a <= 3:
    if a == 1:
        timer = time.time()
        shellsort(lista)
        print("posortowana lista wygląda tak: ", lista)
        break

    elif a == 2:
        timer = time.time()
        mergesort(lista)
        print("posortowana lista wygląda tak: ", lista)
        break

    elif a == 3:
        timer = time.time()
        quicksort(lista)
        print("posortowana lista wygląda tak: ", lista)
        break
    else:
        print("Wybór poza menu")


czas = time.time() - timer

print("Czas działanie programu: ", czas, "sekund")

