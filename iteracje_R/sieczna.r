# iteracje metod� siecznych

iteration <- 30                          
x1 <- rep(0,iteration)                   
x2 <- rep(0,iteration)

f1 <- function(x)                       
{
  y <- 2*x^3-3*x^2+7*x+4
  return(y)
}

f2 <- function(x)
{
  y <- 4*x^5+2*x^3-4*x^2-2*x-5
  return(y)
}

x1[1] <- 0           
x1[2] <- 1           
x2[1] <- 0
x2[2] <- 1
temp <- 0

for(i in 3:iteration) 
{
  temp <- (f2(x2[i-1])*x2[i-2]-f2(x2[i-2])*x2[i-1])/(f2(x2[i-1])-f2(x2[i-2]))
  if(is.na(temp)!=T) x2[i] <- temp
  else break
}

for(i in 3:iteration) 
{
  temp <- (f1(x1[i-1])*x1[i-2]-f1(x1[i-2])*x1[i-1])/(f1(x1[i-1])-f1(x1[i-2]))
  if(is.na(temp)!=T) x1[i] <- temp
  else break
}

rm(temp)