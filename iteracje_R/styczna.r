# iteracja metod� stycznej
iteration <- 20                          

x1 <- rep(0,iteration)                   
x2 <- rep(0,iteration)

f1 <- function(x)                       
{                                       
  y <- 2*x^3-3*x^2+7*x+4
  return(y)
}

fp1 <- function(x)
{
  y <- 6*x^2-6*x+7
}

f2 <- function(x)
{
  y <- 4*x^5+2*x^3-4*x^2-2*x-5
  return(y)
}

fp2 <- function(x)
{
  y <- 20*x^4+6*x^2-8*x-2
}

a <- 0                                  
b <- 1

x1[1] <- a             
x2[1] <- a              

for (i in 2:iteration)   
{
  x1[i] <- x1[i-1]-(f1(x1[i-1])/fp1(x1[i-1]))
  x2[i] <- x2[i-1]-(f2(x2[i-1])/fp2(x2[i-1]))
}