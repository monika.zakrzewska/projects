# iteracje metod� bisekcji Newtona

iteracje <- 15                         

x1 <- rep(0,iteracje)                   
x2 <- rep(0,iteracje)

f1 <- function(x)                       
{
  y <- 2*x^3-3*x^2+7*x+4
  return(y)
}

f2 <- function(x)                       
{
  y <- 4*x^5+2*x^3-4*x^2-2*x-5
  return(y)
}

a <- 0                                  
b <- 1

i <- 1                                  
x <- 0

while (i!=iteracje)           
{
  x <- (a+b)/2                
  if(f1(x)==0.0)              
  {
    break                     
  }
  else if (f1(x)*f1(a)<0)     
  {
    b <- x                    
    i <- i+1
  }                           
  else                        
  {
    a <- x                    
    i <- i+1 
  }
  x1[i-1] <- x                
}
x1[iteracje] <- x             

i <- 1
a <- 0
b <- 1

while (i!=iteracje)           
{
  x <- (a+b)/2
  if(f2(x)==0.0)
  {
    break
  }
  else if (f2(x)*f2(a)<0)
  {
    b <- x
    i <- i+1
  }
  else
  {
    a <- x
    i <- i+1
  }
  x2[i-1] <- x
}
x2[iteracje] <- x
