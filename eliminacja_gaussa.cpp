#include <iostream>
#include <ios>
#include <ostream>
#include <cstdlib>
#include <cstdio>		
#include <ctime>		
#include <cmath>
#include <unistd.h>		
#include <algorithm>
#include <Windows.h>
#include <conio.h>
	
#define N 5

/*Monika Zakrzewska i Agata Wi�niewska
Matematyka Stosowana sem v
Metody numeryczne
Metoda eliminacji Gaussa*/

float Arr[N][N];
float G1[N][N];
float G2[N][N];
float B[N];
float B1[N];
float B2[N];
float L[N][N];
float U[N][N];
float LU[N][N];
int zmienne[N];

void matrix_zmienne()
{
	for(int i=0;i<N;i++)
	{
		zmienne[i]=97+i;
	}
}

int findMax(int k,float A[][N])
{
	float x = Arr[k][k];
	int max=k;
	for(int j=k+1;j<N;j++)
	{
		if(abs(Arr[j][k])>=x)
		{
			x=Arr[j][k];
			max=j;
		}
	}
	return max;
}

void flip(int a,int b,float A[][N])
{
	float temp[N]={0};
	for(int i=0;i<N;i++)
	{
		temp[i]=A[a][i];
		A[a][i]=A[b][i];
		A[b][i]=temp[i];
	}
}

void flipB(int a,int b,float B[])
{
	float temp=0;
	temp=B[a];
	B[a]=B[b];
	B[b]=temp;
}

void Gauss1()
{
	for(int i=0;i<N;i++)
	{
		for(int j=0;j<N;j++)
		{
			G1[i][j]=Arr[i][j];
		}
		B1[i]=B[i];
	}
	float temp=0;
	int index;
	for(int k=0;k<N;k++)
	{
		index=findMax(k,G1);
		flip(k,index,G1);
		flipB(k,index,B1);
		for(int j=k+1;j<N;j++)
		{
			temp=G1[j][k]/G1[k][k];
			B1[j]=B1[j]-temp*B1[k];
			for(int i=0;i<N;i++)
			{
				G1[j][i]=G1[j][i]-temp*G1[k][i];
			}
		}
	}
}

void Gauss2()
{
	for(int i=0;i<N;i++)
	{
		for(int j=0;j<N;j++)
		{
			G2[i][j]=Arr[i][j];
		}
		B2[i]=B[i];
	}
	float temp=0;
	for(int k=0;k<N;k++)
	{
		for(int j=k+1;j<N;j++)
		{
			temp=G2[j][k]/G2[k][k];
			B2[j]=B2[j]-temp*B2[k];
			for(int i=0;i<N;i++)
			{
				G2[j][i]=G2[j][i]-temp*G2[k][i];
			}
		}
	}
}

void LU_f()
{
	for(int i=0;i<N;i++)
	{
		for(int j=0;j<N;j++)
		{
			LU[i][j]=Arr[i][j];
		}
	}
	float temp=0;
	float Ident[N][N]={0};
	for(int i=0;i<N;i++)
	{
		Ident[i][i]=1.0;
	}
	for(int k=0;k<N;k++)
	{
		for(int j=k+1;j<N;j++)
		{
			temp=LU[j][k]/LU[k][k];
			for(int i=0;i<N;i++)
			{
				LU[j][i]=LU[j][i]-temp*LU[k][i];
				Ident[j][i]=Ident[j][i]-temp*Ident[k][i];
			}
		}
	}
	for(int i=0;i<N;i++)
	{
		for(int j=0;j<N;j++)
		{
			L[j][i]=Ident[j][i];
			U[j][i]=LU[j][i];
		}
	}
}

void readMatrix()
{
	int temp;
	FILE *file;
	file = fopen("Array.txt","r");
	if((file=fopen("Array.txt","r"))==NULL)
	{
		printf("Blednie zapisany plik!");
		printf("Prosze zapisac plik do odczytu danych w folderze z programem pod nazwa dict.txt!");
		exit(0);
	}
	for(int i=0;i<N+1;i++)
	{
		for(int j=0;j<N;j++)
		{
			if(i<N)
			{
				fscanf(file,"%d ",&temp);
				Arr[i][j]=temp;
			}
		}
	}
	fscanf(file,"values");
	for(int i=0;i<N;i++)
	{
		fscanf(file,"%d ",&temp);
		B[i]=temp;
	}
	fclose(file);
	printf("\n\n Wczytany uklad:\n\n");
	for(int i=0;i<N;i++)
	{
		printf(" |");
		for(int j=0;j<N;j++)
		{
			printf("% 3.0f ",Arr[i][j]);
		}
		if(i!=0) printf("| |%c|   |%3.0f|",zmienne[i],B[i]);
		if(i==0) printf("| |%c| = |%3.0f|",zmienne[i],B[i]);
		printf("\n");
	}
}

void getMatrix()
{
	printf("\n Podaj macierz glowna ukladu skladajaca sie z liczb naturalnych, \n");
	printf(" elementy wiersza rodzielajac od siebie spacja, \n a wiersze przecinkiem oraz enterem:\n\n");
	for(int i=0;i<N;i++)
	{
		printf(" ");
		for(int j=0;j<N;j++)
		{
			scanf("%f ",&Arr[i][j]);
		}
		scanf(",");
	}
	printf("\n Podaj macierz wartosci rozdzielajac je od siebie przecinkiem i enterem:\n\n");
	for(int i=0;i<N;i++)
	{
		printf(" ");
		scanf("%f,",&B[i]);
	}
}

float R1[N]={0};
float R2[N]={0};

void results()
{
	float temp;
	Gauss1();
	Gauss2();
	float S=0;
	for(int i=N-1;i>=0;i--)
	{
		for(int j=0;j<N;j++)
		{
			if(j!=i) 
			{
				temp = G1[i][j]*R1[j];
				S=S+temp;
			}
		}
		R1[i]=(B1[i]-S)/G1[i][i];
		S=0;
	}
	for(int i=N-1;i>=0;i--)
	{
		for(int j=0;j<i;j++)
		{
			temp = G2[i][j]*R2[j];
			S=S+temp;
		}
		for(int j=i+1;j<N;j++)
		{
			temp = G2[i][j]*R2[j];
			S=S+temp;
		}
		R2[i]=(B2[i]-S)/G2[i][i];
		S=0;
	}
}

int checkCommand(char input[],char command[])
{
	int check=0;
	for(int i=0;i<30;i++)
	{
		if(input[i]!=command[i]) check++;
	}
	if(check==0) return 1;
	else return 0;
}

char com1[30]="enterMatrix";
char com2[30]="LUdecomposition";
char com3[30]="readMatrix";
char com4[30]="Gauss1";
char com5[30]="Gauss2";
char com6[30]="printCurrentMatrix";
char com9[30]="printResults";
char com7[30]="exit";
char com8[30]="help";

int main (void)
{
	int Empty=1;
	matrix_zmienne();
	printf("\n Witaj w programie operujacym na macierzach. \n W celu rozpoczecia dzialania nalezy wprowadzic macierz.\n\n ");
	char x[30]={0};
	scanf("%s",&x);
	while(checkCommand(x,com7)==0)
	{
		if(checkCommand(x,com1)==1)
		{
			Empty=0;
			system("cls");
			printf("\n Rozmiar wprowadzanej macierzy, to %d\n",N);
			printf("\n Aby go zmienic, nalezy w 13 linii kodu redefiniowac N.\n");
			getMatrix();
			system("cls");
			printf("\n\n Podana macierz ukladu:\n\n");
			for(int i=0;i<N;i++)
			{
				printf(" |");
				for(int j=0;j<N;j++)
				{
					printf(" %4.2f ",Arr[i][j]);
				}
				printf("|\n");
			}
			printf("\n\n Podana macierz wartosci:\n\n");
			for(int i=0;i<N;i++)
			{
				printf(" |");
				printf(" %4.2f ",B[i]);
				printf("|\n");
			}
		}
		else if(checkCommand(x,com2)==1&&Empty==0)
		{
			system("cls");
			LU_f();
			printf("\n\n Rozklad LU dla wprowadzonej macierzy:\n\n");
			for(int i=0;i<N;i++)
			{
				printf(" |");
				for(int j=0;j<N;j++)
				{
					printf("% 4.0f ",L[i][j]);
				}
				printf("| |");
				for(int j=0;j<N;j++)
				{
					printf("% 4.0f ",U[i][j]);
				}
				if(i!=0) printf("|   |");
				if(i==0) printf("| = |");
				for(int j=0;j<N;j++)
				{
					printf("% 4.0f ",Arr[i][j]);
				}
				printf("|\n");
			}
		}
		else if(checkCommand(x,com3)==1)
		{
			Empty=0;
			system("cls");
			readMatrix();
		}
		else if(checkCommand(x,com4)==1&&Empty==0)
		{
			system("cls");
			Gauss1();
			printf("\n\n Macierz rozszerzona poddana metodzie eliminacji Gaussa:\n");
			for(int i=0;i<N;i++)
			{
				printf(" |");
				for(int j=0;j<N;j++)
				{
					printf(" %4.2f ",G1[i][j]);
				}
				printf("%c %4.2f |\n",186,B1[i]);
			}
		}
		else if((checkCommand(x,com2)==1||checkCommand(x,com4)==1||checkCommand(x,com5)==1||checkCommand(x,com9)==1)&&Empty==1)
		{
			system("cls");
			printf("\n Jezeli chcesz wykonac operacje na macierzy, najpierw musisz ja wprowadzic!");
		}
		else if(checkCommand(x,com5)==1&&Empty==0) 
		{
			system("cls");
			Gauss2();
			printf("\n\n Macierz poddana metodzie eliminacji Gaussa:\n");
			for(int i=0;i<N;i++)
			{
				printf(" |");
				for(int j=0;j<N;j++)
				{
					printf(" %4.2f ",G2[i][j]);
				}
				printf("%c %4.2f |\n",186,B2[i]);
			}
		}
		else if(checkCommand(x,com6)==1&&Empty==0)
		{
			system("cls");
			printf("\n Obecnie wprowadzona macierz roszerzona:\n");
			for(int i=0;i<N;i++)
			{
				printf(" |");
				for(int j=0;j<N;j++)
				{
					printf(" %4.2f ",Arr[i][j]);
				}
				printf("%c %4.2f |\n",186,B[i]);
			}
		}
		else if(checkCommand(x,com8)==1)
		{
			system("cls");
			printf("\n Lista dostepnych polecen:\n");
			printf("\n  enterMatrix - program pobierze dane od uzytkownika do wygenerowania macierzy");
			printf("\n  readMatrix - program wczytuje graf z pliku Array.txt");
			printf("\n  printCurrentMatrix - program wyswietli obecnie wprowadzona macierz");
			printf("\n  LUdecomposition - program wyswietli rozklad LU");
			printf("\n  Gauss1 - program wyswietli macierz poddana metodzie eliminacj gaussa");
			printf("\n           z zastosowanym wyborem wierszy glownych");
			printf("\n  Gauss2 - program wyswietli macierz poddana metodzie eliminacj gaussa");
			printf("\n           bez zastosowanego wyboru wierszy glownych");
			printf("\n  printResults - program wyswietli wyniki ukladu na podstawie funkcji G1 i G2");
			printf("\n  exit - program zakonczy dzialanie");
		}
		else if(checkCommand(x,com9)==1&&Empty==0)
		{
			results();
			printf("     G1    |    G2    \n");
			printf(" ----------+----------\n");
			for(int i=0;i<N;i++)
			{
				printf(" %3.9f|%3.9f\n",R1[i],R2[i]);
			}
		}
		else
		{
			system("cls");
			printf("\n Obawiam sie, ze nie ma takiej komendy. Sprobuj jeszcze raz!");
			printf("\n Za pomoca komendy help mozesz wyswietlic spis wszystkich dozwolonych komend.");
		}
		for(int i=0;i<30;i++) x[i]=0;
		printf("\n\n Co chcesz dalej zrobic?\n\n ");
		scanf("%s",&x);
	}
	system("cls");
	printf("\n Do widzenia!\n");
}

