#include <stdio.h>
#include <stdlib.h>

/*Politechnika Gda�ska, Wydzia� Fizyki Technicznej i Matematyki Stosowanej
/*Matematyka, 2 rok
/*Algorytmy i struktury danych
/*Laboratorium 1: Proste algorytmy rekurencyjne
/*Monika Zakrzewska
/*28.02.2019

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

	int suma(int n){				// zadeklarowanie czym jest suma i czym jest n w danej funkcji (przy ka�dej nast�pnej funkcji b�dzie analogicznie)
		if(n != 0)					// je�li n jest r�ne od 0, to liczy sum�
			return n+suma(n-1);		//dop�ki n jest r�ne od 0, to dalej liczy
		else 
			return 0;				// jak n = 0, to ko�czy liczy�
	};
	
	int silnia(int n){
		if(n==0) 					//je�li n jest r�wne 0, to ma by� zwr�cona warto�� 1
			return 1;
		else 						//w innym przypadku program ma liczy� silnie i si� jakby cofa do pocz�tku 
			return n*silnia(n-1);	//czyli robi n*(n-1)!, je�li n-1 jes r�ne od 0, to operacja si� powarza, do czasu kiedy n-k=0
	};
	
	int lucas(int n){
		switch(n){ 							//mamy wz�r rekurencyjny, wi�c
			case 1:							//je�li wpiszemy 1, program pokazuje 2
				return 2;					
			case 2:							//je�li wpiszemy 2, program pokazuje 1
				return 1;
		default:							//w innym przypaku program liczy sume liczb lucasa dla n-1 i n-2 i algorytm si� powtarza
			return lucas(n-1)+lucas(n-2);
		}
	};
	
	int fibonacci(int n){
		switch(n){									//podobnie jak we wzorze lucasa, jest to wz�r rekurencyjny
			case 1:									//je�li wybierzemy 1, to pogram zwraca nam warto�� 0
				return 0;							
			case 2:									//je�li wybierzemy 2, to pogram zwraca nam warto�� 1
				return 1;
		default:									//w innym przypadku program liczy sume dw�ch liczb z ci�gu fibonacciego (n-1 i n-2), algorytm si� powtarza
			return fibonacci(n-1)+fibonacci(n-2);
		}
	};
	
	int zamiana(int n){
		if(n>0){			
			zamiana(n/2); 		// w systemie dw�jkowym je�li dzielimy jak�� liczb� na 2, to C robi nam z niej pod�og�, czyli dla kolejnych liczb reszta b�dzie albo 1 albo 0
								// przyk�adowo: 25 (/2) -> 12 r.1, 12 (/2) -> 6 r.0, 6 (/2) -> 3 r.0, 3 (/2) -> 1 r.1, 1 (/2) -> 0 r.1
								// teraz czytamy reszty od ty�u, czyli 25 binarnie jest: 11001
			
			printf("%d", n%2); //n%2, to reszta z dzielenia n/2 (modulo)
		}
	};
	
int main(int argc, char *argv[]) {
	int n,i,S; 				//zdeklarowane zmienne
	int wybor;				//zdeklarowanie wyboru
	
	wybor=1;

	while(wybor != 0){		//dop�ki wyb�r jest r�ny od 0, to p�tla b�dzie si� powtarza�
		
		system("cls");		//czyszczenie konsoli
		
		printf("WYBIERZ MENU: \n");
		printf("\n");
		
		printf(" 0. ZAKONCZ \n");
		printf(" 1. funkcja obliczajaca sume n kolejnych liczb naturalnych \n");
		printf(" 2. funkcja liczaca silnie \n");
		printf(" 3. funkcja obliczajaca n-ta liczbe Lucasa \n");
		printf(" 4. funkcja obliczajaca n-ta liczbe Fibonacciego \n");
		printf(" 5. funkcja zamieniajaca liczbe w zapisie dziesietnym na zapis dwojkowy \n");
		
			scanf("%d", &wybor);		//wczytanie wybranej warto�ci
		
			
			switch(wybor){				//w razie wyboru danej funkcji
				
				case 0:					//je�li wybierzemy 0, to program zwr�ci warto�� 0 i si� wy��czy
					return 0;
				break;
				
				case 1: 							
					printf("Podaj wartosc n: ");	//podajemy warto��
					scanf("%d", &n);				//wczytujemy warto�� n
					printf("Suma = %d\n", suma(n));	//program pokuzje funkcje dla sumy n wyraz�w, przed main jest zdefiniowana funkcja suma(n)
				break;
				
				case 2:
					printf("Podaj liczbe n : ");
					scanf("%d", &n);
					printf("Silnia wynosi %d\n", silnia(n));	//program pokuzje funkcje, kt�ra liczy silnie n wyraz�w, przed main jest zdefiniowana funkcja silnia(n)
					
				break;
				
				case 3:
					printf("Podaj wartosc n: ");
					scanf("%d", &n);
					printf("n-ta liczba Lucasa wynosi: %d\n", lucas(n)); //program pokuzje funkcje dla dla warto�ci n-tej liczy Lucasa, przed main jest zdefiniowana funkcja lucas(n)
					
				break;
				
				case 4:
					printf("Podaj wartosc n: ");
					scanf("%d", &n);
					printf("n-ta liczba ciagu Fibonacciego wynosi : %d\n", fibonacci(n)); //program pokuzje funkcje dla n-tej liczy ci�gu Fibonacciego, przed main jest zdefiniowana funkcja fibonacci(n)
				break;
				
				case 5:
					printf("Podaj wartosc n: ");
					scanf("%d", &n);
					printf("zamienione na system dwojkowy: "); //po przecinku nie mo�e by� nazwy funkcji, bo zwraca ona liczby po kolei, a nie sam� warto��
					zamiana(n);									//dlatego funkcja jest poni�ej
					printf("\n");
				break;
				
				default:								//w przypaku wyboru innego ni� 0-5, program wy�wietla napis
					printf("Wybor poza menu! :( \n");
				break;
				
			}
			system("pause");			//"zatrzymujemy" program, �eby pokazywa� dan� warto�� d�u�ej ni� np. milisekunda
	
	};
	return 0;
}

